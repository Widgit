//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections;
using System.Text;
using System.Windows.Forms;

namespace Widgit
{
    public class NodeSorter : IComparer
    {
        public int Compare(object x, object y)
        {
            TreeNode tx = x as TreeNode;
            TreeNode ty = y as TreeNode;

            // Compare the length of the strings, returning the difference.
            if (tx.Nodes.Count > 0 && ty.Nodes.Count < 1)
                return -1;
            if (tx.Nodes.Count < 1 && ty.Nodes.Count > 0)
                return 1;

            return string.Compare(tx.Text, ty.Text);
        }

    }
}
