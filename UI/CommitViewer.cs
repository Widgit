//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Git;

namespace Widgit
{
    public partial class CommitViewer : Form
    {
        protected Commit m_commit;
        public CommitViewer(Commit c)
        {
            m_commit = c;
            InitializeComponent();
            m_id.Text = c.ID;
            m_author.Text = c.AuthorName;
            m_email.Text = c.AuthorEmail;
            m_date.Text = c.Date;
            m_message.Text = c.Subject + Repo.LineSeparator + Repo.LineSeparator + c.Body;
            m_parents.Items.AddRange(c.Parents.ToArray());
        }

        private void OnShowParent(object sender, EventArgs e)
        {
            Commit c = new Commit(m_commit.Path, m_parents.SelectedItem.ToString());
            CommitViewer v = new CommitViewer(c);
            v.Show();
        }

        private void OnDone(object sender, EventArgs e)
        {
            Close();
        }
    }
}