//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Git;

namespace Widgit
{
    public partial class RevisionChooser : Form
    {
        Repo m_currentRepo;
        public RevisionChooser()
        {
            InitializeComponent();
        }

        private void OnBranchSelected(object sender, EventArgs e)
        {
            m_tags.SelectedIndex = 0;
            if (!String.IsNullOrEmpty(m_branches.SelectedItem.ToString()))
            {
                m_statusTable.Treeish = ((Branch)m_branches.SelectedItem);
                m_statusTable.RepopulateTable();
            }
        }

        private void OnTagSelected(object sender, EventArgs e)
        {
            m_branches.SelectedIndex = 0;
            if (!String.IsNullOrEmpty(m_tags.SelectedItem.ToString()))
            {
                m_statusTable.Treeish = ((Tag)m_tags.SelectedItem);
                m_statusTable.RepopulateTable();
            }
        }

        public string Choose(Repo repo)
        {
            return Choose(repo, null);
        }

        public string Choose(Repo repo, List<string> paths)
        {
            m_currentRepo = repo;
            m_statusTable.SetRepo(m_currentRepo);
            m_branches.Items.Clear();
            m_branches.Items.Add("");
            m_branches.Items.AddRange(repo.Branches.ToArray());
            m_tags.Items.Clear();
            m_tags.Items.Add("");
            m_tags.Items.AddRange(repo.Tags.ToArray());
            m_branches.SelectedValue = m_currentRepo.CurrentBranch.ToString();
            m_statusTable.Treeish = m_currentRepo.CurrentBranch;
            if (null != paths)
            {
                m_statusTable.Paths = paths;
            }
            if (DialogResult.OK == ShowDialog())
            {
                if (m_statusTable.SelectedItems.Count == 1)
                {
                    return ((Treeish)m_statusTable.SelectedItems[0].Tag).ID;
                }
            }
            return null;
        }
    }
}