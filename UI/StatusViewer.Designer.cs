//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

namespace Widgit
{
    partial class StatusViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_statusList = new System.Windows.Forms.ListView();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.m_toolbar = new Widgit.RepoToolStrip();
            this.m_opMenu = new Widgit.FileOpMenu();
            this.SuspendLayout();
            // 
            // m_statusList
            // 
            this.m_statusList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7});
            this.m_statusList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_statusList.Location = new System.Drawing.Point(0, 25);
            this.m_statusList.Name = "m_statusList";
            this.m_statusList.Size = new System.Drawing.Size(482, 304);
            this.m_statusList.TabIndex = 2;
            this.m_statusList.UseCompatibleStateImageBehavior = false;
            this.m_statusList.View = System.Windows.Forms.View.Details;
            this.m_statusList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnRowClicked);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "File";
            this.columnHeader7.Width = 478;
            // 
            // m_toolbar
            // 
            this.m_toolbar.CheckedMask = -1;
            this.m_toolbar.Location = new System.Drawing.Point(0, 0);
            this.m_toolbar.Name = "m_toolbar";
            this.m_toolbar.Size = new System.Drawing.Size(482, 25);
            this.m_toolbar.StateMask = -1;
            this.m_toolbar.TabIndex = 3;
            this.m_toolbar.Text = "repoToolStrip1";
            // 
            // m_opMenu
            // 
            this.m_opMenu.Name = "fileOpMenu1";
            this.m_opMenu.Size = new System.Drawing.Size(204, 202);
            // 
            // StatusViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_statusList);
            this.Controls.Add(this.m_toolbar);
            this.Name = "StatusViewer";
            this.Size = new System.Drawing.Size(482, 329);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView m_statusList;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private RepoToolStrip m_toolbar;
        private FileOpMenu m_opMenu;
    }
}
