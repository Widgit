//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

namespace Widgit
{
    partial class CommitTable
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.m_fetchCommits = new System.Windows.Forms.ToolStripButton();
            this.m_nextCommits = new System.Windows.Forms.ToolStripButton();
            this.m_previousCommits = new System.Windows.Forms.ToolStripButton();
            this.m_filterCommits = new System.Windows.Forms.ToolStripButton();
            this.m_commitsView = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.toolStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip3
            // 
            this.toolStrip3.AllowMerge = false;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_fetchCommits,
            this.m_nextCommits,
            this.m_previousCommits,
            this.m_filterCommits});
            this.toolStrip3.Location = new System.Drawing.Point(0, 0);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(388, 25);
            this.toolStrip3.TabIndex = 2;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // m_fetchCommits
            // 
            this.m_fetchCommits.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_fetchCommits.Enabled = false;
            this.m_fetchCommits.Image = global::Widgit.Properties.Resources.lightning;
            this.m_fetchCommits.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_fetchCommits.Name = "m_fetchCommits";
            this.m_fetchCommits.Size = new System.Drawing.Size(23, 22);
            this.m_fetchCommits.Text = "toolStripButton7";
            this.m_fetchCommits.ToolTipText = "Fetch";
            this.m_fetchCommits.Click += new System.EventHandler(this.OnFetchCommits);
            // 
            // m_nextCommits
            // 
            this.m_nextCommits.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.m_nextCommits.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_nextCommits.Enabled = false;
            this.m_nextCommits.Image = global::Widgit.Properties.Resources.resultset_next;
            this.m_nextCommits.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_nextCommits.Name = "m_nextCommits";
            this.m_nextCommits.Size = new System.Drawing.Size(23, 22);
            this.m_nextCommits.Text = "Next";
            this.m_nextCommits.Click += new System.EventHandler(this.OnNextCommits);
            // 
            // m_previousCommits
            // 
            this.m_previousCommits.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.m_previousCommits.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_previousCommits.Enabled = false;
            this.m_previousCommits.Image = global::Widgit.Properties.Resources.resultset_previous;
            this.m_previousCommits.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_previousCommits.Name = "m_previousCommits";
            this.m_previousCommits.Size = new System.Drawing.Size(23, 22);
            this.m_previousCommits.Text = "Previous";
            this.m_previousCommits.Click += new System.EventHandler(this.OnPreviousCommits);
            // 
            // m_filterCommits
            // 
            this.m_filterCommits.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_filterCommits.Enabled = false;
            this.m_filterCommits.Image = global::Widgit.Properties.Resources.application_view_list;
            this.m_filterCommits.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_filterCommits.Name = "m_filterCommits";
            this.m_filterCommits.Size = new System.Drawing.Size(23, 22);
            this.m_filterCommits.Text = "Filter";
            this.m_filterCommits.Click += new System.EventHandler(this.OnShowFilter);
            // 
            // m_commitsView
            // 
            this.m_commitsView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.m_commitsView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_commitsView.FullRowSelect = true;
            this.m_commitsView.Location = new System.Drawing.Point(0, 25);
            this.m_commitsView.MultiSelect = false;
            this.m_commitsView.Name = "m_commitsView";
            this.m_commitsView.Size = new System.Drawing.Size(388, 252);
            this.m_commitsView.TabIndex = 3;
            this.m_commitsView.UseCompatibleStateImageBehavior = false;
            this.m_commitsView.View = System.Windows.Forms.View.Details;
            this.m_commitsView.ItemActivate += new System.EventHandler(this.OnShowCommit);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Commit ID";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Author";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Email";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Date";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Subject";
            this.columnHeader5.Width = 438;
            // 
            // CommitTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_commitsView);
            this.Controls.Add(this.toolStrip3);
            this.Name = "CommitTable";
            this.Size = new System.Drawing.Size(388, 277);
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton m_fetchCommits;
        private System.Windows.Forms.ToolStripButton m_nextCommits;
        private System.Windows.Forms.ToolStripButton m_previousCommits;
        private System.Windows.Forms.ToolStripButton m_filterCommits;
        private System.Windows.Forms.ListView m_commitsView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
    }
}
