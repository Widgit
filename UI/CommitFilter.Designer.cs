namespace Widgit
{
    partial class CommitFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_authorsCheck = new System.Windows.Forms.CheckBox();
            this.m_dateCheck = new System.Windows.Forms.CheckBox();
            this.m_after = new System.Windows.Forms.DateTimePicker();
            this.m_before = new System.Windows.Forms.DateTimePicker();
            this.m_authors = new System.Windows.Forms.TextBox();
            this.m_pathsCheck = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.m_treeView = new Widgit.RepoTreeView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_authorsCheck
            // 
            this.m_authorsCheck.AutoSize = true;
            this.m_authorsCheck.Location = new System.Drawing.Point(12, 25);
            this.m_authorsCheck.Name = "m_authorsCheck";
            this.m_authorsCheck.Size = new System.Drawing.Size(65, 17);
            this.m_authorsCheck.TabIndex = 0;
            this.m_authorsCheck.Text = "Authors:";
            this.m_authorsCheck.UseVisualStyleBackColor = true;
            // 
            // m_dateCheck
            // 
            this.m_dateCheck.AutoSize = true;
            this.m_dateCheck.Location = new System.Drawing.Point(12, 59);
            this.m_dateCheck.Name = "m_dateCheck";
            this.m_dateCheck.Size = new System.Drawing.Size(57, 17);
            this.m_dateCheck.TabIndex = 1;
            this.m_dateCheck.Text = "Dates:";
            this.m_dateCheck.UseVisualStyleBackColor = true;
            // 
            // m_after
            // 
            this.m_after.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_after.Location = new System.Drawing.Point(109, 87);
            this.m_after.Name = "m_after";
            this.m_after.Size = new System.Drawing.Size(298, 20);
            this.m_after.TabIndex = 2;
            // 
            // m_before
            // 
            this.m_before.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_before.Location = new System.Drawing.Point(109, 113);
            this.m_before.Name = "m_before";
            this.m_before.Size = new System.Drawing.Size(298, 20);
            this.m_before.TabIndex = 3;
            // 
            // m_authors
            // 
            this.m_authors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_authors.Location = new System.Drawing.Point(109, 25);
            this.m_authors.Name = "m_authors";
            this.m_authors.Size = new System.Drawing.Size(301, 20);
            this.m_authors.TabIndex = 4;
            // 
            // m_pathsCheck
            // 
            this.m_pathsCheck.AutoSize = true;
            this.m_pathsCheck.Location = new System.Drawing.Point(13, 160);
            this.m_pathsCheck.Name = "m_pathsCheck";
            this.m_pathsCheck.Size = new System.Drawing.Size(56, 17);
            this.m_pathsCheck.TabIndex = 5;
            this.m_pathsCheck.Text = "Paths:";
            this.m_pathsCheck.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "After:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Before:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.m_treeView);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 188);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(422, 206);
            this.panel1.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 149);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(422, 57);
            this.panel2.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(254, 21);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnCancel);
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(335, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnOk);
            // 
            // m_treeView
            // 
            this.m_treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_treeView.ImageList = null;
            this.m_treeView.Location = new System.Drawing.Point(0, 0);
            this.m_treeView.Name = "m_treeView";
            this.m_treeView.ShowOperations = true;
            this.m_treeView.Size = new System.Drawing.Size(422, 149);
            this.m_treeView.TabIndex = 0;
            // 
            // CommitFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 394);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_pathsCheck);
            this.Controls.Add(this.m_authors);
            this.Controls.Add(this.m_before);
            this.Controls.Add(this.m_after);
            this.Controls.Add(this.m_dateCheck);
            this.Controls.Add(this.m_authorsCheck);
            this.Name = "CommitFilter";
            this.Text = "CommitFilter";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox m_authorsCheck;
        private System.Windows.Forms.CheckBox m_dateCheck;
        private System.Windows.Forms.DateTimePicker m_after;
        private System.Windows.Forms.DateTimePicker m_before;
        private System.Windows.Forms.TextBox m_authors;
        private System.Windows.Forms.CheckBox m_pathsCheck;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private RepoTreeView m_treeView;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}