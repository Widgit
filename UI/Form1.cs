//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Git;

namespace Widgit
{ 
    public partial class Form1 : Form
    {
        protected OutputTextWriter m_outputWriter;
        Prefs m_prefs = new Prefs();
        Repo m_currentRepo;
        ImageList m_imageList = new ImageList();

        public FileUpdater m_fileUpdater;

        public Form1()
        {
            InitializeComponent();
            //TextWriter stdout = System.Console.Out;
            //OutputTextWriter m_outputWriter = new OutputTextWriter(m_outputArea, stdout);
            //System.Console.SetOut(m_outputWriter);            
            m_fileUpdater = new FileUpdater(m_prefs);
            PopulateImageList();
            Executioner.BinaryDirPath = m_prefs.GitDir;
            SetReposList();

            m_repoViewer.FileUpdater = m_fileUpdater;
            m_repoViewer.ImageList = m_imageList;
            m_statusViewer.FileUpdater = m_fileUpdater;
            m_statusViewer.ImageList = m_imageList;
            
        }

        protected void PopulateImageList()
        {
            System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            System.Resources.ResourceManager m = new System.Resources.ResourceManager("Widgit.Properties.Resources", a);
            Image im = (System.Drawing.Image)m.GetObject("folder");
            m_imageList.Images.Add("folder", im);

            im = (System.Drawing.Image)m.GetObject("page");
            m_imageList.Images.Add("normal", im);
            im = (System.Drawing.Image)m.GetObject("page_edit");
            m_imageList.Images.Add("modified", im);
            im = (System.Drawing.Image)m.GetObject("page_add");
            m_imageList.Images.Add("added", im);
            im = (System.Drawing.Image)m.GetObject("page_delete");
            m_imageList.Images.Add("delete", im);
            im = (System.Drawing.Image)m.GetObject("page_error");
            m_imageList.Images.Add("unmerged", im);
            im = (System.Drawing.Image)m.GetObject("disconnect");
            m_imageList.Images.Add("unknown", im);

            im = (System.Drawing.Image)m.GetObject("page_error");
            m_imageList.Images.Add("broken", im);
            im = (System.Drawing.Image)m.GetObject("page_lightning");
            m_imageList.Images.Add("modechanged", im);
            im = (System.Drawing.Image)m.GetObject("page_copy");
            m_imageList.Images.Add("copied", im);
            im = (System.Drawing.Image)m.GetObject("page_go");
            m_imageList.Images.Add("renamed", im);

            m_imageList.Images.Add("empty", new Bitmap(16, 16));
        }

        bool SetReposList()
        {
            if (m_prefs.Repos == null)
            {
                m_prefs.Repos = new System.Collections.Specialized.StringCollection();
            }
            object o = m_reposCombo.SelectedItem;
            m_reposCombo.Items.Clear();
            foreach (string s in m_prefs.Repos)
            {
                m_reposCombo.Items.Add(s);
            }
            m_reposCombo.SelectedItem = o;
            return true;
        }

        void PreferencesUpdated()
        {
            Executioner.BinaryDirPath = m_prefs.GitDir;
            SetReposList();
        }

        protected bool UpdateTags()
        {
            throw new NotImplementedException();
        }

        protected bool UpdateBranches()
        {
            m_branchesCombo.Items.Clear();
            foreach (Branch b in m_currentRepo.Branches)
            {
                m_branchesCombo.Items.Add(b.Name);
            }
            m_branchesCombo.SelectedItem = m_currentRepo.CurrentBranch.Name;
            return true;
        }

        private void OnPreferencesClicked(object sender, EventArgs e)
        {
            PreferencesEditor p = new PreferencesEditor(m_prefs);
            p.ShowDialog();
            PreferencesUpdated();
        }

        private void OnManageRepos(object sender, EventArgs e)
        {
            RepoManager m = new RepoManager(m_prefs);
            m.ShowDialog();
            SetReposList();
        }

        private void OnRepoChanged(object sender, EventArgs e)
        {
            if (m_reposCombo.SelectedIndex > -1)
            {
                m_currentRepo = new Repo(m_reposCombo.SelectedItem.ToString());
                m_fileUpdater.SetRepo(m_currentRepo);
                m_repoViewer.SetRepo(m_currentRepo, true);
                UpdateBranches();
                //UpdateTags();
                m_commitTable.SetRepo(m_currentRepo);
                m_statusViewer.SetRepo(m_currentRepo, false);
            }
            else
            {
                MessageBox.Show("Select a repo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnBranchChanged(object sender, EventArgs e)
        {
            if (m_branchesCombo.SelectedIndex > -1)
            {
                if (m_currentRepo.CurrentBranch.Name != m_branchesCombo.SelectedItem.ToString())
                {
                    foreach (Branch b in m_currentRepo.Branches)
                    {
                        if (b.Name == m_branchesCombo.SelectedItem.ToString())
                        {
                            m_currentRepo.Checkout(b);
                        }
                    }
                    m_repoViewer.SetRepo(m_currentRepo, true);
                }
            }
            else
            {
                MessageBox.Show("Select a branch.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnCreateBranch(object sender, EventArgs e)
        {

        }

        private void OnMergeBranch(object sender, EventArgs e)
        {

        }

        private void OnDeleteBranch(object sender, EventArgs e)
        {

        }

        private void OnExit(object sender, EventArgs e)
        {

        }

        private void OnResetHard(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Are you sure you want to revert everything you have in your working directory?",
                                                "Widgit", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                m_currentRepo.Reset(null);
                m_repoViewer.SetRepo(m_currentRepo, true);
            }
        }
    }
}
