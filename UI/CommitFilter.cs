using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Git;

namespace Widgit
{
    public partial class CommitFilter : Form, ILogFilter
    {
        Repo m_currentRepo;

        public ImageList ImageList { get { return m_treeView.ImageList; } set { m_treeView.ImageList = value; } }
        public FileUpdater FileUpdater { set { m_treeView.FileUpdater = value; } }

        protected List<string> m_authorsList = new List<string>();
        public List<string> Authors { get { return m_authorsList; } }

        protected string m_afterDate = "";
        public string After { get { return m_afterDate; } }

        protected string m_beforeDate = "";
        public string Before { get { return m_beforeDate; } }

        protected List<string> m_paths = new List<string>();
        public List<string> Paths { get { return m_paths; } set { m_paths = value; } }

        public int Max { get { return 100; } }

        protected Treeish m_start;
        public Treeish Treeish { get { return m_start; } set { m_start = value; } }

        public CommitFilter(Repo repo)
        {
            InitializeComponent();
            m_treeView.ShowOperations = false;
            m_currentRepo = repo;
            m_treeView.SetRepo(repo, false);
        }

        private void OnCancel(object sender, EventArgs e)
        {
            Close();
        }

        private void OnOk(object sender, EventArgs e)
        {
            m_authorsList.Clear();
            if (m_authorsCheck.Checked)
            {
                string[] authors = m_authors.Text.Split(",".ToCharArray());
                foreach (string s in authors)
                {
                    m_authorsList.Add(s.Trim());
                }
            }
            m_afterDate = "";
            m_beforeDate = "";
            if (m_dateCheck.Checked)
            {
                m_afterDate = m_after.Value.ToShortDateString();
                m_beforeDate = m_before.Value.ToShortDateString();
            }
            m_paths.Clear();
            if (m_pathsCheck.Checked)
            {
                foreach (File f in m_treeView.GetSelected())
                {
                    m_paths.Add(f.PathWithName);
                }
            }
        }
    }
}