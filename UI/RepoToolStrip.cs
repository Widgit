//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Git;

namespace Widgit
{
    using Props = global::Widgit.Properties.Resources;

    public partial class RepoToolStrip : ToolStrip, IFileOperations
    {
        public event SelectedStatesChangedHandler StatesChanged;
        public event EventHandler Add;
        public event EventHandler Copy;
        public event EventHandler Delete;
        public event EventHandler RenameOrMove;
        public event EventHandler Revert;
        public event EventHandler RefreshFiles;
#pragma warning disable 0067
        public event EventHandler DiffHead;
        public event EventHandler DiffOther;
        public event EventHandler View;
        public event EventHandler ViewPrevious;
#pragma warning restore 0067

        protected int m_stateMask = -1;
        public int StateMask { get { return m_stateMask; } set { m_stateMask = value; } }

        protected int m_checkedMask = -1;
        public int CheckedMask { get { return m_checkedMask; } set { m_checkedMask = value; } }

        private ToolStripDropDownButton m_typeSelector = new ToolStripDropDownButton("Select Types To Show", Props.page);
        private ToolStripMenuItem m_normal = new ToolStripMenuItem("Up to date", Props.page);
        private ToolStripMenuItem m_added = new ToolStripMenuItem("Added", Props.page_add);
        private ToolStripMenuItem m_modified = new ToolStripMenuItem("Modified", Props.page_edit);
        private ToolStripMenuItem m_unmergedOrBroken = new ToolStripMenuItem("Unmerged/Broken", Props.page_error);
        private ToolStripMenuItem m_copied = new ToolStripMenuItem("Copied", Props.page_copy);
        private ToolStripMenuItem m_renamed = new ToolStripMenuItem("Renamed", Props.page_go);
        private ToolStripMenuItem m_deleted = new ToolStripMenuItem("Deleted", Props.delete);
        private ToolStripMenuItem m_unmanaged = new ToolStripMenuItem("Unmanaged", Props.disconnect);

        private ToolStripButton m_refresh = new ToolStripButton("Refresh", Props.arrow_refresh);
        private ToolStripButton m_addFile = new ToolStripButton("Add", Props.page_add);
        private ToolStripButton m_copyFiles = new ToolStripButton("Copy...", Props.page_copy);
        private ToolStripButton m_moveRename = new ToolStripButton("Move/Rename...", Props.page_go);
        private ToolStripButton m_revertFiles = new ToolStripButton("Revert", Props.arrow_redo);
        private ToolStripButton m_removeFiles = new ToolStripButton("Delete", Props.delete);

        public RepoToolStrip()
        {
            SuspendLayout();
            Items.AddRange(
                new System.Windows.Forms.ToolStripItem[] {
                                                            m_addFile,
                                                            m_copyFiles,
                                                            m_moveRename,
                                                            m_revertFiles,
                                                            m_removeFiles,
                                                            m_typeSelector,
                                                            m_refresh
                                                         }
                                            );
            Location = new System.Drawing.Point(0, 0);

            m_typeSelector.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            m_typeSelector.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            m_typeSelector.DropDownItems.AddRange(
                new System.Windows.Forms.ToolStripItem[] {
                                                            m_normal,
                                                            m_added,
                                                            m_modified,
                                                            m_unmergedOrBroken,
                                                            m_copied,
                                                            m_renamed,
                                                            m_deleted,
                                                            m_unmanaged,
                                                          }
                                                  );
            m_typeSelector.ImageTransparentColor = System.Drawing.Color.Magenta;
            m_typeSelector.Size = new System.Drawing.Size(29, 22);
            ResumeLayout();
        }

        public bool Initialize(int showMask, int checkedMask)
        {
            SuspendLayout();
            m_checkedMask = checkedMask;
            m_stateMask = showMask;
            InitMenuItem(m_normal, Git.FileState.Normal);
            InitMenuItem(m_added, Git.FileState.Added);
            InitMenuItem(m_modified, Git.FileState.Modified);
            InitMenuItem(m_unmergedOrBroken, Git.FileState.Unmerged);
            InitMenuItem(m_copied, Git.FileState.Copied);
            InitMenuItem(m_renamed, Git.FileState.Renamed);
            InitMenuItem(m_deleted, Git.FileState.Deleted);
            InitMenuItem(m_unmanaged, Git.FileState.Unknown);

            InitButton(m_refresh, ToolStripItemAlignment.Right, OnRefresh);
            InitButton(m_addFile, ToolStripItemAlignment.Left, OnAdd);
            InitButton(m_copyFiles, ToolStripItemAlignment.Left, OnCopy);
            InitButton(m_moveRename, ToolStripItemAlignment.Left, OnMoveRename);
            InitButton(m_revertFiles, ToolStripItemAlignment.Left, OnRevert);
            InitButton(m_removeFiles, ToolStripItemAlignment.Left, OnDelete);
            ResumeLayout();
            return true;
        }

        protected void InitButton(ToolStripButton b, ToolStripItemAlignment a, EventHandler h)
        {
            b.Alignment = a;
            b.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            b.Size = new System.Drawing.Size(23, 22);
            b.Click += h;
        }

        protected void InitMenuItem(ToolStripMenuItem i, FileState state)
        {
            i.Checked = 0 != (m_checkedMask & (int)state);
            i.CheckOnClick = true;
            i.Size = new System.Drawing.Size(171, 22);
            i.Visible = 0 != (m_stateMask & (int)state);
            i.CheckedChanged += OnStatesChanged;
        }

        protected void OnStatesChanged(object sender, EventArgs e)
        {
            int states = 0;
            states |= m_normal.Checked ? (int)Git.FileState.Normal : 0;
            states |= m_added.Checked ? (int)Git.FileState.Added : 0;
            states |= m_modified.Checked ? (int)Git.FileState.Modified : 0;
            states |= m_modified.Checked ? (int)Git.FileState.ModeChanged : 0;
            states |= m_unmergedOrBroken.Checked ? (int)Git.FileState.Broken : 0;
            states |= m_unmergedOrBroken.Checked ? (int)Git.FileState.Unmerged : 0;
            states |= m_copied.Checked ? (int)Git.FileState.Copied : 0;
            states |= m_renamed.Checked ? (int)Git.FileState.Renamed : 0;
            states |= m_deleted.Checked ? (int)Git.FileState.Deleted : 0;
            states |= m_unmanaged.Checked ? (int)Git.FileState.Unknown : 0;
            if (null != StatesChanged)
            {
                StatesChanged(this, new SelectedStatesChangedEvent(states));
            }
        }

        protected void OnRefresh(object sender, EventArgs e)
        {
            FireEvent(RefreshFiles);
        }

        protected void OnAdd(object sender, EventArgs e)
        {
            FireEvent(Add);
        }

        protected void OnCopy(object sender, EventArgs e)
        {
            FireEvent(Copy);
        }

        protected void OnMoveRename(object sender, EventArgs e)
        {
            FireEvent(RenameOrMove);
        }

        protected void OnRevert(object sender, EventArgs e)
        {
            FireEvent(Revert);
        }

        protected void OnDelete(object sender, EventArgs e)
        {
            FireEvent(Delete);
        }

        protected void FireEvent(EventHandler h)
        {
            if (h != null)
            {
                h(this, new EventArgs());
            }
        }

        public void EnableModifyButtons(bool bEnable)
        {
            m_addFile.Enabled = bEnable;
            m_removeFiles.Enabled = bEnable;
            m_revertFiles.Enabled = bEnable;
            m_moveRename.Enabled = bEnable;
            m_copyFiles.Enabled = bEnable;
        }
    }
}
