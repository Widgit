//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Widgit
{
    using Props = global::Widgit.Properties.Resources;

    class FileOpMenu : ContextMenuStrip, IFileOperations
    {
#pragma warning disable 0067
        public event SelectedStatesChangedHandler StatesChanged;
        public event EventHandler RefreshFiles;
#pragma warning restore 0067
        public event EventHandler Add;
        public event EventHandler Copy;
        public event EventHandler Delete;
        public event EventHandler RenameOrMove;
        public event EventHandler Revert;
        public event EventHandler DiffHead;
        public event EventHandler DiffOther;
        public event EventHandler View;
        public event EventHandler ViewPrevious;

        protected ToolStripMenuItem m_view = new ToolStripMenuItem("View", Props.page_white_magnify);
        protected ToolStripMenuItem m_viewPrevious = new ToolStripMenuItem("View previous revision...", Props.page_white_magnify);
        protected ToolStripMenuItem m_diffHead = new ToolStripMenuItem("Diff against HEAD", Props.page_code);
        protected ToolStripMenuItem m_diffOther = new ToolStripMenuItem("Diff against Other...", Props.page_code);
        private ToolStripMenuItem m_addFile = new ToolStripMenuItem("Add", Props.page_add);
        private ToolStripMenuItem m_copyFiles = new ToolStripMenuItem("Copy...", Props.page_copy);
        private ToolStripMenuItem m_moveRename = new ToolStripMenuItem("Move/Rename...", Props.page_go);
        private ToolStripMenuItem m_revertFiles = new ToolStripMenuItem("Revert", Props.arrow_redo);
        private ToolStripMenuItem m_removeFiles = new ToolStripMenuItem("Delete", Props.delete);

        public FileOpMenu()
        {
            SuspendLayout();

            Items.Add(m_view);
            Items.Add(m_viewPrevious);
            Items.Add(m_diffHead);
            Items.Add(m_diffOther);
            Items.Add(m_addFile);
            Items.Add(m_copyFiles);
            Items.Add(m_moveRename);
            Items.Add(m_revertFiles);
            Items.Add(m_removeFiles);
            m_view.Click += OnView;
            m_viewPrevious.Click += OnViewPrevious;
            m_diffHead.Click += OnDiffHead;
            m_diffOther.Click += OnDiffOther;
            m_addFile.Click += OnAdd;
            m_copyFiles.Click += OnCopy;
            m_moveRename.Click += OnMove;
            m_revertFiles.Click += OnRevert;
            m_removeFiles.Click += OnDelete;

            ResumeLayout();
        }

        protected void OnView(object sender, EventArgs e)
        {
            FireEvent(View);
        }

        protected void OnViewPrevious(object sender, EventArgs e)
        {
            FireEvent(ViewPrevious);
        }

        protected void OnDiffHead(object sender, EventArgs e)
        {
            FireEvent(DiffHead);
        }

        protected void OnDiffOther(object sender, EventArgs e)
        {
            FireEvent(DiffOther);
        }

        protected void OnAdd(object sender, EventArgs e)
        {
            FireEvent(Add);
        }

        protected void OnCopy(object sender, EventArgs e)
        {
            FireEvent(Copy);
        }

        protected void OnMove(object sender, EventArgs e)
        {
            FireEvent(RenameOrMove);
        }

        protected void OnRevert(object sender, EventArgs e)
        {
            FireEvent(Revert);
        }

        protected void OnDelete(object sender, EventArgs e)
        {
            FireEvent(Delete);
        }

        protected void FireEvent(EventHandler h)
        {
            if (h != null)
            {
                h(this, new EventArgs());
            }
        }
    }
}
