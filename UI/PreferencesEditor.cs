//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Widgit
{
    public partial class PreferencesEditor : Form
    {
        internal Prefs m_prefs;

        internal PreferencesEditor(Prefs p)
        {
            m_prefs = p;
            InitializeComponent();
            m_gitPath.Text = m_prefs.GitDir;
            m_editor.Text = m_prefs.ViewerApp;
            m_editorArgs.Text = m_prefs.ViewerArgs;
            m_difftool.Text = m_prefs.DiffApp;
            m_difftoolArgs.Text = m_prefs.DiffArgs;
            m_mergeTool.Text = m_prefs.MergeApp;
            m_mergetoolArgs.Text = m_prefs.MergeArgs;
        }

        protected string ChooseDir(bool bDirs)
        {
            string s = "";
            if (bDirs)
            {
                m_folderChooser.ShowDialog();
                s = m_folderChooser.SelectedPath;
            }
            else
            {
                m_fileChooser.ShowDialog();
                s = m_fileChooser.FileName;
            }
            return s;
        }

        private void OnChooseMinGWPath(object sender, EventArgs e)
        {
            string dir = ChooseDir(true);
            if (!String.IsNullOrEmpty(dir))
            {
                m_gitPath.Text = dir;
            }
        }

        private void OnSave(object sender, EventArgs e)
        {
            string dir = m_gitPath.Text;
            if (!System.IO.File.Exists(dir + System.IO.Path.DirectorySeparatorChar + "git-init-db") &&
                    !System.IO.File.Exists(dir + System.IO.Path.DirectorySeparatorChar + "git-init-db.exe"))
            {
                MessageBox.Show("The location does not appear to have git executables in it.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            m_prefs.GitDir = dir;
            m_prefs.DiffApp = m_difftool.Text;
            m_prefs.DiffArgs = m_difftoolArgs.Text;
            m_prefs.ViewerApp = m_editor.Text;
            m_prefs.ViewerArgs = m_editorArgs.Text;
            m_prefs.MergeApp = m_mergeTool.Text;
            m_prefs.MergeArgs = m_mergetoolArgs.Text;
            m_prefs.Save();
            Close();
        }

        private void OnBrowseEditor(object sender, EventArgs e)
        {
            string dir = ChooseDir(false);
            if (!String.IsNullOrEmpty(dir))
            {
                m_editor.Text = dir;
            }
        }

        private void OnBrowseDifftool(object sender, EventArgs e)
        {
            string dir = ChooseDir(false);
            if (!String.IsNullOrEmpty(dir))
            {
                m_difftool.Text = dir;
            }
        }

        private void OnBrowseMegetool(object sender, EventArgs e)
        {
            string dir = ChooseDir(false);
            if (!String.IsNullOrEmpty(dir))
            {
                m_mergeTool.Text = dir;
            }
        }
    }
}