//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

namespace Widgit
{
    partial class RepoTreeView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_workspaceTree = new System.Windows.Forms.TreeView();
            this.m_toolbar = new Widgit.RepoToolStrip();
            this.m_opMenu = new Widgit.FileOpMenu();
            this.SuspendLayout();
            // 
            // m_workspaceTree
            // 
            this.m_workspaceTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_workspaceTree.Location = new System.Drawing.Point(0, 25);
            this.m_workspaceTree.Name = "m_workspaceTree";
            this.m_workspaceTree.Size = new System.Drawing.Size(388, 519);
            this.m_workspaceTree.TabIndex = 5;
            this.m_workspaceTree.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.OnNodeActivated);
            this.m_workspaceTree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.OnNodeClicked);
            // 
            // m_toolbar
            // 
            this.m_toolbar.CheckedMask = -1;
            this.m_toolbar.Location = new System.Drawing.Point(0, 0);
            this.m_toolbar.Name = "m_toolbar";
            this.m_toolbar.Size = new System.Drawing.Size(388, 25);
            this.m_toolbar.StateMask = -1;
            this.m_toolbar.TabIndex = 6;
            this.m_toolbar.Text = "repoToolStrip1";
            // 
            // m_opMenu
            // 
            this.m_opMenu.Name = "m_opMenu";
            // 
            // RepoTreeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_workspaceTree);
            this.Controls.Add(this.m_toolbar);
            this.Name = "RepoTreeView";
            this.Size = new System.Drawing.Size(388, 544);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView m_workspaceTree;
        private Widgit.RepoToolStrip m_toolbar;
        private FileOpMenu m_opMenu;
    }
}
