//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Git;

namespace Widgit
{
    public partial class CommitTable : UserControl
    {
        protected Commit m_lastCommitInPage;
        protected Stack<Commit> m_topCommits = new Stack<Commit>();
        protected Repo m_currentRepo;
        protected CommitFilter m_filter;

        public ListView.SelectedListViewItemCollection SelectedItems { get { return m_commitsView.SelectedItems; } }

        protected Treeish m_treeish;
        public Treeish Treeish { get { return m_treeish; } set { m_treeish = value; } }

        public List<string> Paths
        {
            get { return m_filter.Paths; }
            set
            {
                if (null != value)
                {
                    m_filter.Paths = value;
                }
            }
        }

        public CommitTable()
        {
            InitializeComponent();
        }

        public void SetRepo(Repo repo)
        {
            m_currentRepo = repo;
            m_filter = new CommitFilter(repo);
            m_fetchCommits.Enabled = true;
            m_filterCommits.Enabled = true;
            m_commitsView.Items.Clear();
            m_previousCommits.Enabled = false;
            m_nextCommits.Enabled = false;
            m_topCommits.Clear();
            m_lastCommitInPage = null;
        }

        private void OnFetchCommits(object sender, EventArgs e)
        {
            RepopulateTable();
        }

        public void RepopulateTable()
        {
            List<Commit> commits;
            m_filter.Treeish = m_treeish;
            if (!m_currentRepo.GetLog(m_filter, out commits))
            {
                MessageBox.Show("Could not get commits.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            m_topCommits.Clear();
            if (m_topCommits.Count > 0)
            {
                m_topCommits.Push(commits[0]);
            }
            FillCommitList(commits);
        }

        private void FillCommitList(List<Commit> commits)
        {
            m_commitsView.BeginUpdate();
            m_commitsView.Items.Clear();
            foreach (Commit c in commits)
            {
                ListViewItem i = new ListViewItem(new string[] { c.ID, c.AuthorName, c.AuthorEmail, c.Date, c.Subject });
                i.Tag = c;
                m_commitsView.Items.Add(i);
            }
            if (commits.Count > 0)
            {
                m_lastCommitInPage = commits[commits.Count - 1];
            }
            m_previousCommits.Enabled = m_nextCommits.Enabled = commits.Count != 0;
            if (m_commitsView.Items.Count != 100)
            {
                m_nextCommits.Enabled = false;
            }
            if (m_topCommits.Count < 2)
            {
                m_previousCommits.Enabled = false;
            }

            m_commitsView.EndUpdate();
        }

        private void OnPreviousCommits(object sender, EventArgs e)
        {
            List<Commit> commits;
            m_topCommits.Pop();
            m_filter.Treeish = m_topCommits.Peek();
            if (!m_currentRepo.GetLog(m_filter, out commits))
            {
                m_nextCommits.Enabled = false;
                return;
            }
            if (commits.Count < 1)
            {
                m_previousCommits.Enabled = false;
            }
            else
            {
                FillCommitList(commits);
            }
        }

        private void OnNextCommits(object sender, EventArgs e)
        {
            List<Commit> commits;
            m_filter.Treeish = m_lastCommitInPage;
            if (!m_currentRepo.GetLog(m_filter, out commits))
            {
                m_nextCommits.Enabled = false;
                return;
            }
            if (commits.Count < 1)
            {
                m_nextCommits.Enabled = false;
            }
            else
            {
                m_topCommits.Push(commits[0]);
                FillCommitList(commits);
            }
        }

        private void OnShowCommit(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection items = m_commitsView.SelectedItems;
            if (items.Count == 1)
            {
                CommitViewer c = new CommitViewer((Commit)items[0].Tag);
                c.Show();
            }
        }

        private void OnShowFilter(object sender, EventArgs e)
        {
            if (DialogResult.OK == m_filter.ShowDialog())
            {
                RepopulateTable();
            }
        }
    }
}
