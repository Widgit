//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Git;

namespace Widgit
{
    public partial class RepoTreeView : UserControl, ISelectable
    {
        ImageList m_imageList;
        public ImageList ImageList { get { return m_imageList; } set { m_imageList = value; } }

        TreeNode m_rootNode;
        protected Dictionary<string, string> m_expandedPaths = new Dictionary<string, string>();
        protected int m_selectedTypes = -1;
        Repo m_currentRepo;
        protected Dictionary<string, Git.File> m_knownFiles = new Dictionary<string,Git.File>();

        protected bool m_showOperations = true;
        public bool ShowOperations
        {
            get { return m_showOperations; }
            set
            {
                m_toolbar.EnableModifyButtons(value);
                m_showOperations = value;
            }
        }
        
        protected FileUpdater m_updater;
        public FileUpdater FileUpdater
        {
            set
            {
                if (null != m_updater)
                {
                    m_updater.FilesUpdated -= OnFilesUpdated;
                    m_updater[m_toolbar] = null;
                }
                m_updater = value;
                m_updater.FilesUpdated += OnFilesUpdated;
                m_updater[m_toolbar] = (ISelectable)this;
                m_updater[m_opMenu] = (ISelectable)this;
            }
        }

        public RepoTreeView()
        {
            InitializeComponent();
            m_workspaceTree.BeforeExpand += new TreeViewCancelEventHandler(OnBeforeExpand);
            m_workspaceTree.AfterCollapse += new TreeViewEventHandler(OnAfterCollapse);
            m_workspaceTree.TreeViewNodeSorter = new Widgit.NodeSorter();
            m_workspaceTree.FullRowSelect = true;
            m_workspaceTree.AfterSelect += new TreeViewEventHandler(OnNodeSelected);
            m_toolbar.Initialize(-1, -1);
            m_toolbar.RefreshFiles += OnRefreshTree;
            m_toolbar.StatesChanged += OnStatesChanged;
        }

        void OnStatesChanged(RepoToolStrip sender, SelectedStatesChangedEvent e)
        {
            m_selectedTypes = e.SelectedStates;
            OnRefreshTree(sender, e);
        }

        void OnNodeSelected(object sender, TreeViewEventArgs e)
        {
            m_toolbar.EnableModifyButtons(m_workspaceTree.SelectedNode != null);
        }

        void OnAfterCollapse(object sender, TreeViewEventArgs e)
        {
            if (Enabled)
            {
                m_expandedPaths.Remove((string)e.Node.Tag);
            }
        }

        protected void AddNewFileNode(TreeNode n, Git.File f)
        {
            if (((int)f.State & m_selectedTypes) != 0)
            {
                TreeNode subNode = n.Nodes.Add(f.Name, f.Name);
                subNode.Tag = f;
                subNode.ImageKey = subNode.SelectedImageKey = Git.File.GetStateName(f.State);
            }
        }

        protected TreeNode AddNewDirNode(TreeNode n, string subpath, string tag)
        {
            TreeNode subNode = n.Nodes.Add(subpath, subpath);
            subNode.ImageIndex = 0;
            subNode.Tag = tag;
            subNode.Nodes.Add(new TreeNode());
            return subNode;
        }

        void OnBeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            m_workspaceTree.SelectedNode = e.Node;
            string path = (string)e.Node.Tag;
            if (Enabled && !m_expandedPaths.ContainsKey(path))
            {
                m_expandedPaths.Add((string)e.Node.Tag, null);
            }
            if (e.Node.FirstNode.Tag != null)
            {
                return;
            }
            e.Node.Nodes.Clear();
            m_workspaceTree.BeginUpdate();
            DirectoryInfo dir = new DirectoryInfo(m_currentRepo.Path + Repo.DirectorySeparator + path);
            FileInfo[] files = dir.GetFiles();
            DirectoryInfo[] subDirs = dir.GetDirectories();
            foreach (DirectoryInfo subDir in subDirs)
            {
                string fullPath = subDir.Name;
                if (!String.IsNullOrEmpty(path))
                {
                    fullPath = path + Repo.DirectorySeparator + subDir.Name;
                }
                if (path.Length == 0 && fullPath == m_currentRepo.GitDir)
                {
                    continue;
                }
                AddNewDirNode(e.Node, subDir.Name, fullPath);
            }

            foreach (FileInfo file in files)
            {
                string fullPath = file.Name;
                if (!String.IsNullOrEmpty(path))
                {
                    fullPath = path + Repo.DirectorySeparator + file.Name;
                }
                Git.File f;
                if (!m_knownFiles.TryGetValue(fullPath, out f))
                {
                    f = new Git.File(m_currentRepo.Path, fullPath);
                    f.State = FileState.Normal;
                }
                AddNewFileNode(e.Node, f);
            }
            if (e.Node.Nodes.Count == 0)
            {
                TreeNode n = new TreeNode();
                n.ImageKey = "empty";
                n.SelectedImageKey = null;
                e.Node.Nodes.Add(n);
            }
            m_workspaceTree.EndUpdate();
        }

        public bool SetRepo(Repo repo, bool bLoadNow)
        {
            m_currentRepo = repo;
            m_workspaceTree.BeginUpdate();
            m_workspaceTree.Nodes.Clear();
            m_workspaceTree.ImageList = m_imageList;
            if (bLoadNow)
            {
                m_rootNode = new TreeNode("root");
                m_rootNode.Tag = "";
                m_rootNode.ImageIndex = 0;
                m_workspaceTree.Nodes.Add(m_rootNode);
                m_rootNode.Nodes.Add(new TreeNode());
                m_currentRepo.GetFilesMap(out m_knownFiles, true);
            }
            m_workspaceTree.EndUpdate();
            m_toolbar.Enabled = true;
            return true;
        }

        protected void ExpandPath(string s)
        {
            string[] parts = s.Split(Repo.DirectorySeparator.ToCharArray());
            TreeNode currentNode = m_rootNode;
            m_rootNode.Expand();
            foreach (string part in parts)
            {
                int idx = currentNode.Nodes.IndexOfKey(part);
                if (idx > -1)
                {
                    currentNode = currentNode.Nodes[idx];
                    currentNode.Expand();
                }
            }
        }

        protected void OnRefreshTree(object sender, EventArgs e)
        {
            SetRepo(m_currentRepo, true);
            this.Enabled = false;
            foreach (string s in m_expandedPaths.Keys)
            {
                ExpandPath(s);
            }
            this.Enabled = true;
        }

        

        protected bool FindNode(Git.File f, out TreeNode n)
        {
            if (!String.IsNullOrEmpty(f.RelativePath))
            {
                string[] pathParts = f.RelativePath.Split(Repo.DirectorySeparator.ToCharArray());
                TreeNode curNode = m_rootNode;
                foreach (string part in pathParts)
                {
					int idx = curNode.Nodes.IndexOfKey(part);
                    curNode = curNode.Nodes[idx];
                }
				int nodeIdx = curNode.Nodes.IndexOfKey(f.Name);
                n = curNode.Nodes[nodeIdx];
            }
            else
            {
				int idx = m_rootNode.Nodes.IndexOfKey(f.Name);
                n = m_rootNode.Nodes[idx];
            }
            return true;
        }

        private void OnFilesUpdated(FileUpdater sender, FilesUpdatedEvent evt)
        {
            List<Git.File> files = evt.FilesList;
            foreach(Git.File f in files)
            {
                TreeNode n;

                if (0 != ((int)f.State & m_selectedTypes) && FindNode(f, out n))
                {
                    n.Tag = f;
                    n.ImageKey = n.SelectedImageKey = Git.File.GetStateName(f.State);
                }
            }
        }

        public List<Git.File> GetSelected()
        {
            List<Git.File> files = new List<Git.File>();
            if (m_workspaceTree.SelectedNode.Tag is Git.File)
            {
                files.Add((Git.File)m_workspaceTree.SelectedNode.Tag);
            }
            return files;
        }

        public void Clear()
        {
            m_expandedPaths.Clear();
        }

        private void OnNodeActivated(object sender, TreeNodeMouseClickEventArgs e)
        {

        }

        private void OnNodeClicked(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right && m_showOperations)
            {
                m_workspaceTree.SelectedNode = e.Node;
                m_opMenu.Show(m_workspaceTree, e.X, e.Y);
            }
        }
    }
}
