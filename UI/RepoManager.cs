//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Widgit
{
    public partial class RepoManager : Form
    {
        internal Prefs m_prefs;
        protected List<string> m_paths;

        internal RepoManager(Prefs prefs)
        {
            InitializeComponent();
            m_prefs = prefs;
            m_paths = new List<string>();
            foreach (string path in m_prefs.Repos)
            {
                m_paths.Add(path);
            }
            m_paths.Sort();
            m_pathsList.Items.AddRange(m_paths.ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult r = m_folderChooser.ShowDialog();
            if (r == DialogResult.OK)
            {
                if (!Git.Repo.ValidateRepo(m_folderChooser.SelectedPath))
                {
                    MessageBox.Show("The location does not appear to be a git repository.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (m_paths.Contains(m_folderChooser.SelectedPath))
                {
                    MessageBox.Show("You have already added " + m_folderChooser.SelectedPath + " to your repositories list",
                                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    m_paths.Add(m_folderChooser.SelectedPath);
                    m_prefs.Repos.Clear();
                    foreach (string s in m_paths)
                    {
                        m_prefs.Repos.Add(s);
                    }
                    m_prefs.Save();
                    m_pathsList.Items.Clear();
                    m_pathsList.Items.AddRange(m_paths.ToArray());
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (m_pathsList.SelectedIndex > -1)
            {
                m_paths.Remove(m_pathsList.SelectedItem.ToString());
                m_prefs.Repos.Remove(m_pathsList.SelectedItem.ToString());
                m_prefs.Save();
                m_pathsList.Items.Remove(m_pathsList.SelectedItem);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}