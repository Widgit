//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Git;

namespace Widgit
{
    public partial class StatusViewer : UserControl, ISelectable
    {
        Repo m_currentRepo;
        Dictionary<string, Git.File> m_files;
        ImageList m_imageList;
        public ImageList ImageList { get { return m_imageList; } set { m_imageList = value; m_statusList.SmallImageList = m_imageList; } }

        public ListView.SelectedListViewItemCollection SelectedItems { get { return m_statusList.SelectedItems; } }

        public bool Multiselect { get { return m_statusList.MultiSelect; } set { m_statusList.MultiSelect = value; } }

        protected FileUpdater m_updater;
        public FileUpdater FileUpdater
        {
            set
            {
                if (null != m_updater)
                {
                    m_updater.FilesUpdated -= OnFilesUpdated;
                    m_updater[m_toolbar] = null;
                    m_updater[m_opMenu] = null;
                }
                m_updater = value;
                m_updater.FilesUpdated += OnFilesUpdated;
                m_updater[m_toolbar] = (ISelectable)this;
                m_updater[m_opMenu] = (ISelectable)this;
            }
        }

        protected int m_selectedTypes = -1 & (int)~Git.FileState.Unknown & (int)~Git.FileState.Normal;

        public StatusViewer()
        {
            InitializeComponent();
            m_toolbar.Initialize(-1 & (int)~FileState.Normal, -1 & (int)~FileState.Normal & (int)~FileState.Unknown);
            m_toolbar.RefreshFiles += OnRefreshTree;
            m_toolbar.StatesChanged += OnStatesChanged;
            m_toolbar.Enabled = false;
        }

        public void SetRepo(Repo repo, bool bRefresh)
        {
            m_currentRepo = repo;
            m_currentRepo.GetFilesMap(out m_files, bRefresh);
            PopulateTable();
            m_toolbar.Enabled = true;
        }

        protected void PopulateTable()
        {
            m_statusList.BeginUpdate();
            m_statusList.Items.Clear();
            foreach (KeyValuePair<string, Git.File> pair in m_files)
            {
                if (0 == (m_selectedTypes & (int)pair.Value.State))
                {
                    continue;
                }
                ListViewItem item = new ListViewItem( new string[] { pair.Key }, Git.File.GetStateName(pair.Value.State));
                item.Tag = pair.Value;
                item.Name = pair.Key;
                m_statusList.Items.Add(item);
            }
            m_statusList.EndUpdate();
        }

        private void OnFilesUpdated(FileUpdater sender, FilesUpdatedEvent evt)
        {
            List<Git.File> files = evt.FilesList;
            foreach (Git.File f in files)
            {
                m_files[f.PathWithName] = f;
            }
            PopulateTable();
        }

        private void OnRefreshTree(object sender, EventArgs e)
        {
            if (null != m_currentRepo)
            {
                m_currentRepo.GetFilesMap(out m_files, true);
                PopulateTable();
            }
        }

        private void OnStatesChanged(RepoToolStrip sender, SelectedStatesChangedEvent e)
        {
            m_selectedTypes = e.SelectedStates;
            PopulateTable();
        }

        public List<File> GetSelected()
        {
            List<File> files = new List<File>();
            foreach (ListViewItem i in m_statusList.SelectedItems)
            {
                files.Add((File)i.Tag);
            }
            return files;
        }

        private void OnRowClicked(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ListViewItem i = m_statusList.GetItemAt(e.X, e.Y);
                m_statusList.SelectedItems.Clear();
                i.Selected = true;
                m_opMenu.Show(m_statusList, e.X, e.Y);
            }
        }
    }
}
