//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;


using System;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.ComponentModel;

namespace Widgit
{
	
	
	public class OutputTextWriter : System.IO.StringWriter
	{
        delegate void SetTextCallback(char[] buffer, int index, int count);

	    ListBox m_listBox;
	    TextWriter m_realConsole;
		public OutputTextWriter(ListBox b, TextWriter w)
		{
		    m_listBox = b;
		    m_realConsole = w;
		}

        public override void Write(char[] buffer, int index, int count)
        {
            if (!m_listBox.InvokeRequired)
            {
                string s = new string(buffer);
                int start = index > -1 ? index : 0;
                int newcount = count < ((buffer.Length - 1) - start) ? count : ((buffer.Length - 1) - start);
                m_listBox.Items.Add(s.Substring(index, newcount).Trim());
                Application.DoEvents();
            }
            
            m_realConsole.Write(buffer, index, count);
        }
		public override void Write(string value)
		{
		    m_listBox.Items.Add(value);
            m_realConsole.Write(value + System.Environment.NewLine);
		    Application.DoEvents();
		}
		
		public void DebugWrite(string value)
		{
		    m_listBox.Items.Add(value);
		    Application.DoEvents();
		}
	}
}
