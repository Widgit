//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;


using System;

namespace Git
{
	public enum FileState
	{
        Normal = 1,
	    Added = 1<<1,
	    Copied = 1<<2,
	    Deleted = 1<<3,
	    Modified = 1<<4,
	    Renamed = 1<<5,
        ModeChanged = 1<<6,
        Unmerged = 1<<7,
        Unknown = 1<<8,
        Broken = 1<<9
	}
	
	public class File : IComparable<File>
	{
	    protected string m_mode;
	    public string Mode { get { return m_mode; } set { m_mode = value; } }
	    
	    protected string m_hash;
		public string ID { get { return m_hash; } }

        protected string m_repoPath;
		
		protected string m_relPath; 
		public string RelativePath { get { return m_relPath; } }

        protected string m_pathWithName;
        public string PathWithName { get { return m_pathWithName; } }
		
		protected string m_name; 
		public string Name { get { return m_name; } }
		
		protected int m_stage;
		public int Stage
		{
		    get
		    {
		        return m_stage;
		    }
		    
		    set
		    {
		        if (value < 0 || value > 3)
		        {
		            m_stage = value;
		        }
		        m_stage = value;
		    }
		}
		
		protected FileState m_state = FileState.Unknown;
		public FileState State
		{
		    get
		    {
		        return m_state;
		    }
		    
		    set
		    {
		        m_state = value;
		    }
		}
		
		
		public File(string repoPath, string relativePath)
		{
            m_repoPath = repoPath;
            int idx = relativePath.LastIndexOf(Repo.DirectorySeparator);
            if (idx < 0)
            {
                m_relPath = "";
                m_name = relativePath;
            }
            else
            {
                m_relPath = relativePath.Substring(0, idx);
                m_name = relativePath.Substring(idx + 1);
            }
            m_pathWithName = relativePath;
		}
		
		public bool SetStage(string val)
	    {
	        int i;
	        if (!int.TryParse(val, out i))
	        {
	            return false;
	        }
	        if (m_stage < 0 || m_stage > 3)
	        {
	            return false;
	        }
	        m_stage = i;
	        return true;
	    }
	    
	    public int CompareTo(File other)
	    {
	        return m_name.CompareTo(other.m_name);
	    }
	    
        /*************
         * Added (A)
         * Copied (C) Has 2 Files, source, dest
         * Deleted (D)
         * Modified (M)
         * Renamed (R) Has 2 Files, source, dest
         * mode changed (T)
         * Unmerged (U)
         * Unknown (X)
         * Broken (B)
         * ************/
	    public void SetState(string val)
	    {
	        val = val.Trim();
	        switch(val)
	        {
	            case "A":
                   m_state = FileState.Added;
                   break;
                case "C":
                   m_state = FileState.Copied;
                   break;
                case "D":
                   m_state = FileState.Deleted;
                   break;
                case "M":
                   m_state = FileState.Modified;
                   break;
                case "R":
                   m_state = FileState.Renamed;
                   break;
                case "T":
                   m_state = FileState.ModeChanged;
                   break;
                case "U":
                   m_state = FileState.Unmerged;
                   break;
                case "X":
                   m_state = FileState.Unknown;
                   break;
                case "B":
                   m_state = FileState.Broken;
                   break;
               default:
                   m_state = FileState.Unknown;
                   break;
           }
	    }

        public static string GetStateName(FileState state)
        {
            switch (state)
            {
                case FileState.Normal:
                    return "normal";
                case FileState.Deleted:
                    return "delete";
                case FileState.Modified:
                    return "modified";
                case FileState.Unknown:
                    return "unknown";
                case FileState.Unmerged:
                    return "unmerged";
                case FileState.Added:
                    return "added";
                case FileState.Copied:
                    return "copied";
                case FileState.ModeChanged:
                    return "modechanged";
                case FileState.Broken:
                    return "broken";
                case FileState.Renamed:
                    return "renamed";
            }
            return "unknown";
        }

        public bool GetRevision(out string contents)
        {
            return GetRevision("HEAD", out contents);
        }

        public bool GetRevision(string commitish, out string contents)
        {
            if (String.IsNullOrEmpty(commitish))
            {
                contents = null;
                return false;
            }
            Executioner e = Executioner.GetExecutioner(m_repoPath, true);

            string error;
            return 0 == e.Execute("git-show", commitish + ":" + m_pathWithName, "", out contents, out error);
        }
	}
}
