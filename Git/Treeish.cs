//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;


using System;

namespace Git
{
    public abstract class Treeish
	{
        public abstract string ID { get; internal set; }
        public abstract string Name { get; internal set; }
	    
	    protected string m_path;
        public string Path { get { return m_path; } }
	    
	    public Treeish(string path)
	    {
	        m_path = path;
	    }
	    
	    public virtual bool Verify()
	    {
	        Executioner e = Executioner.GetExecutioner(m_path, false);
            return 0 == e.Execute("git-rev-parse", "--verify " + ID);
	    }
	}
}
