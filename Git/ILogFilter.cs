using System;
using System.Collections.Generic;
using System.Text;

namespace Git
{
    public interface ILogFilter
    {
        List<string> Authors { get; }
        string After { get; }
        string Before { get; }
        List<string> Paths { get; }
        Treeish Treeish { get; }
        int Max { get; }
    }
}
