//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.Text;

namespace Widgit
{
    public class Multidict<KeyType, ValueType> : System.Collections.IEnumerable
    {
        protected Dictionary<KeyType, List<ValueType>> m_dict = new Dictionary<KeyType, List<ValueType>>();

        public Multidict()
        {
        }

        public List<ValueType> Get(KeyType key)
        {
            List<ValueType> items;
            if (m_dict.TryGetValue(key, out items))
            {
                return items;
            }
            return null;
        }

        public bool Add(KeyType key, ValueType value)
        {
            return Add(key, value, false);
        }

        public bool Add(KeyType key, ValueType value, bool bAllowDups)
        {
            List<ValueType> items;
            if (!m_dict.TryGetValue(key, out items))
            {
                items = new List<ValueType>();
                m_dict[key] = items;
            }
            if (!items.Contains(value) || bAllowDups)
            {
                items.Add(value);
                return true;
            }
            return false;
        }

        public System.Collections.IEnumerator GetEnumerator()
        {
            return m_dict.GetEnumerator();
        }
    }
}
