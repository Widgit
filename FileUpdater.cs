//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.Text;
using Git;

namespace Widgit
{
    using FilesList = List<Git.File>;

    public class FilesUpdatedEvent : EventArgs
    {
        public FilesUpdatedEvent(FilesList files)
        {
            m_files = new FilesList(files);
        }
        protected FilesList m_files;
        public FilesList FilesList { get { return new FilesList(m_files); } }
    }

    public delegate void FileUpdatedHandler(FileUpdater sender, FilesUpdatedEvent evt);

    public class FileUpdater
    {
        public event FileUpdatedHandler FilesUpdated;

        Prefs m_prefs;
        
        protected Dictionary<IFileOperations, ISelectable> m_EventSourcesToSelectables = new Dictionary<IFileOperations,ISelectable>();
        public ISelectable this[IFileOperations o]
        {
            get { return m_EventSourcesToSelectables[o]; }
            set
            {
                if (null == value)
                {
                    o.Add -= OnAddFiles;
                    o.Copy -= OnCopyFiles;
                    o.Delete -= OnDeleteFiles;
                    o.RenameOrMove -= OnMoveFiles;
                    o.Revert -= OnRevertFiles;
                    o.DiffHead -= OnDiffFile;
                    o.DiffOther -= OnDiffHistoryFile;
                    o.View -= OnViewFile;
                    o.ViewPrevious -= OnViewPreviousFile;
                    m_EventSourcesToSelectables.Remove(o);
                }
                else
                {
                    m_EventSourcesToSelectables[o] = value;
                    o.Add += OnAddFiles;
                    o.Copy += OnCopyFiles;
                    o.Delete += OnDeleteFiles;
                    o.RenameOrMove += OnMoveFiles;
                    o.Revert += OnRevertFiles;
                    o.DiffHead += OnDiffFile;
                    o.DiffOther += OnDiffHistoryFile;
                    o.View += OnViewFile;
                    o.ViewPrevious += OnViewPreviousFile;
                }
            }
        }


        Repo m_currentRepo;

        internal FileUpdater(Prefs prefs)
        {
            m_prefs = prefs;
        }

        public void AddEventSource(IFileOperations f, ISelectable s)
        {
            m_EventSourcesToSelectables[f] = s;
            f.Add += OnAddFiles;
            f.Copy += OnCopyFiles;
            f.Delete += OnDeleteFiles;
            f.RenameOrMove += OnMoveFiles;
            f.Revert += OnRevertFiles;
        }

        public void SetRepo(Repo repo)
        {
            m_currentRepo = repo;
        }

        public bool AddFiles(FilesList files)
        {
            List<string> paths = MakePathsList(files);
            FilesList updatedFiles;
            if (m_currentRepo.Add(paths, out updatedFiles) && files.Count > 0)
            {
                OnFileUpdated(new FilesUpdatedEvent(updatedFiles));
            }
            return true;
        }

        public bool DeleteFiles(FilesList files)
        {
            List<string> paths = MakePathsList(files);
            FilesList updatedFiles;
            if (m_currentRepo.Remove(paths, out updatedFiles) && files.Count > 0)
            {
                OnFileUpdated(new FilesUpdatedEvent(updatedFiles));
            }
            return true;
        }

        public bool RevertFiles(FilesList files)
        {
            List<string> paths = MakePathsList(files);
            FilesList updatedFiles;
            if (m_currentRepo.Revert(paths, out updatedFiles) && files.Count > 0)
            {
                OnFileUpdated(new FilesUpdatedEvent(updatedFiles));
            }
            return true;
        }

        private static List<string> MakePathsList(FilesList files)
        {
            List<string> paths = new List<string>();
            foreach (File f in files)
            {
                if (f.RelativePath.Length > 0)
                {
                    paths.Add(f.PathWithName);
                }
                else
                {
                    paths.Add(f.Name);
                }
            }
            return paths;
        }

        protected void OnFileUpdated(FilesUpdatedEvent e)
        {
            if (FilesUpdated != null)
            {
                FilesUpdated(this, e);
            }
        }

        protected void OnAddFiles(object sender, EventArgs e)
        {
            ISelectable s = m_EventSourcesToSelectables[(IFileOperations)sender];
            AddFiles(s.GetSelected());
        }

        protected void OnDeleteFiles(object sender, EventArgs e)
        {
            ISelectable s = m_EventSourcesToSelectables[(IFileOperations)sender];
            DeleteFiles (s.GetSelected());
        }

        protected void OnRevertFiles(object sender, EventArgs e)
        {
            ISelectable s = m_EventSourcesToSelectables[(IFileOperations)sender];
            RevertFiles(s.GetSelected());
        }

        protected void OnViewFile(object sender, EventArgs e)
        {
            ISelectable s = m_EventSourcesToSelectables[(IFileOperations)sender];
            foreach (File f in s.GetSelected())
            {
                string path = SerializeRevision(f, null);
                
                if (String.IsNullOrEmpty(m_prefs.ViewerApp))
                {
                    System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
                    si.FileName = path;
                    si.UseShellExecute = true;
                    System.Console.WriteLine(si.FileName + " " + si.Arguments);
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo = si;
                    p.Start();
                }
                else
                {
                    LaunchApp(m_prefs.ViewerApp, m_prefs.ViewerArgs, path, "", "", "");
                }
            }
        }

        protected void OnViewPreviousFile(object sender, EventArgs e)
        {
            ISelectable s = m_EventSourcesToSelectables[(IFileOperations)sender];
            RevertFiles(s.GetSelected());
        }

        protected void OnDiffFile(object sender, EventArgs e)
        {
            ISelectable s = m_EventSourcesToSelectables[(IFileOperations)sender];
            if (String.IsNullOrEmpty(m_prefs.DiffApp))
            {
                return;
            }
            foreach (File f in s.GetSelected())
            {
                Diff(f, null, f, "HEAD");
            }
        }

        protected void OnDiffHistoryFile(object sender, EventArgs e)
        {
            ISelectable s = m_EventSourcesToSelectables[(IFileOperations)sender];
            if (String.IsNullOrEmpty(m_prefs.DiffApp))
            {
                return;
            }
            
            List<Git.File> files = s.GetSelected();
            if (files.Count < 0)
            {
                return;
            }
            List<string> paths = new List<string>();
            foreach (File f in files)
            {
                paths.Add(f.PathWithName);
            }
            RevisionChooser c = new RevisionChooser();
            string commitish = c.Choose(m_currentRepo, paths);
            if (null == commitish)
            {
                return;
            }
            foreach (File f in files)
            {
                Diff(f, null, f, commitish);
            }
        }


        private string ReplaceOrAppend(string orig, string toReplace, string replaceWith)
        {
            if (orig.IndexOf(toReplace) < 0)
            {
                orig += " \"" + replaceWith + "\""; 
            }
            else
            {
                orig = orig.Replace("[File]", "\"" + replaceWith + "\"");
            }
            return orig;
        }

        protected string SerializeRevision(File f, string commitish)
        {
            string contents;
            if (String.IsNullOrEmpty(commitish))
            {
                string nativePath = f.PathWithName.Replace(Repo.DirectorySeparator,
                                                            System.IO.Path.DirectorySeparatorChar.ToString());
                nativePath = m_currentRepo.Path + System.IO.Path.DirectorySeparatorChar + nativePath;
                return nativePath;
            }
            if (!f.GetRevision(commitish, out contents))
            {
                return null;
            }
            string tmpFileName = System.IO.Path.GetTempFileName();
            System.IO.StreamWriter writer = new System.IO.StreamWriter(tmpFileName);
            writer.Write(contents);
            writer.Close();
            return tmpFileName;
        }

        protected bool Diff(File file1, string commitish1, File file2, string commitish2)
        {
            if (null == file2)
            {
                file2 = file1;
            }

            string nativePath1 = SerializeRevision(file1, commitish1);
            if (null == nativePath1)
            {
                return false;
            }

            string nativePath2 = SerializeRevision(file2, commitish2);
            if (null == nativePath2)
            {
                return false;
            }
            return LaunchApp(m_prefs.DiffApp, m_prefs.DiffArgs, "", nativePath2, nativePath1, "");
        }

        protected bool LaunchApp(string execPath, string args, string file, string yours, string theirs, string target)
        {
            System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
            si.FileName = execPath;
            args = ReplaceOrAppend(args, "[File]", file);
            args = ReplaceOrAppend(args, "[Theirs]", theirs);
            args = ReplaceOrAppend(args, "[Yours]", yours);
            args = ReplaceOrAppend(args, "[Target]", target);
            si.Arguments = args;
            System.Console.WriteLine(si.FileName + " " + si.Arguments);
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo = si;
            p.Start();
            return true;
        }

        protected void OnCopyFiles(object sender, EventArgs e)
        {
            //ISelectable s = m_EventSourcesToSelectables[sender];
            //CopyFiles(s.GetSelected());
        }

        protected void OnMoveFiles(object sender, EventArgs e)
        {
            //ISelectable s = m_EventSourcesToSelectables[sender];
            //RevertFiles(s.GetSelected()); ;
        }
    }
}
