//Widgit is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.

//Widgit is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with Widgit; if not, write to the Free Software
//Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USAusing System;

using System;
using System.Collections.Generic;
using System.Text;

namespace Widgit
{
    public class SelectedStatesChangedEvent : EventArgs
    {
        public SelectedStatesChangedEvent(int selectedStates)
        {
            m_selectedStates = selectedStates;
        }
        protected int m_selectedStates;
        public int SelectedStates { get { return m_selectedStates; } }
    }

    public delegate void SelectedStatesChangedHandler(RepoToolStrip sender, SelectedStatesChangedEvent evt);

    public interface IFileOperations
    {
        event SelectedStatesChangedHandler StatesChanged;
        event EventHandler Add;
        event EventHandler Copy;
        event EventHandler Delete;
        event EventHandler RenameOrMove;
        event EventHandler Revert;
        event EventHandler RefreshFiles;
        event EventHandler DiffHead;
        event EventHandler DiffOther;
        event EventHandler View;
        event EventHandler ViewPrevious;
    }
}
